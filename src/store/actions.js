
import Axios from 'axios'

const getPlaceHolderData = ({commit}) => new Promise((resolve,reject) => {
	setTimeout(() => {
		Axios.get('https://jsonplaceholder.typicode.com/todos?_limit=5')
			.then((response) => {
				resolve(response);
			})
			.catch((err) => {
				const payload = {
					status : true,
					message: 'Server Error',
					messageType: 'error'
				}
				commit('UPDATE_SNACKBAR', {payload});
				reject(err);
			});
	}, 2000);
});

export default {
	getPlaceHolderData
};