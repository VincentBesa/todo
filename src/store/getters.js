const todos = state => state.todos;
const snackBar = state => state.snackBar;
const snackBarMessage = state => state.snackBarMessage;
const messagetype = state => state.messagetype;

export default {
    todos,
    snackBar,
    snackBarMessage,
    messagetype
};