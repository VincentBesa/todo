const UPDATE_TODOS = (state, data) => {
    state.todos = data
    localStorage.setItem('todos', JSON.stringify(state.todos))
}

const REMOVE_COMPLETED_TASKS = (state) => {
    state.todos = state.todos.filter(task => !task.completed);
}

const UPDATE_SNACKBAR = (state, data) => {
    state.snackBar = data.payload.status
    state.snackBarMessage = data.payload.message
    state.messagetype = data.payload.messageType
}

export default {
    UPDATE_TODOS,
    REMOVE_COMPLETED_TASKS,
    UPDATE_SNACKBAR
};